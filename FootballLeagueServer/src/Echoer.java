import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;

public class Echoer extends Thread {
	
	String adminUsername = "sperchez";
	String adminPassword = "12345";
	
	private Socket socket;
	
	/**
	 * Public constructor for the Echoer class
	 * @param socket
	 */
	public Echoer(Socket socket) {
		this.socket = socket;
	}

	/**
     * Verifies if the username and passwords that were communicated by the user are correct.
     * The accounts are stored in a HashMap. 
     * Depending on the corectness of the info that was transmitted, it will communicate a message. 
	 */
	public void run() {
		try {
			HashMap<String, String> loginAccounts = new HashMap<String, String>();
			loginAccounts.put(adminUsername, adminPassword);
			
			BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter output = new PrintWriter(socket.getOutputStream(), true);
			
			String username = input.readLine();
			String password = input.readLine();
			if(loginAccounts.containsKey(username) && loginAccounts.get(username).equals(password))
				output.println("Login info correct!");
			else
				output.println("Login info incorrect!");
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}