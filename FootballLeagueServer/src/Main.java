import java.io.IOException;
import java.net.ServerSocket;

public class Main {

	public static void main(String[] args) {
		ServerSocket serverSocket;
		try {
			serverSocket = new ServerSocket(1000);
			while (true) {
				new Echoer(serverSocket.accept()).start();
				System.out.println("Started server.");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
