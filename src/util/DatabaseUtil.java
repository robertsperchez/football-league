package util;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import javafx.scene.image.Image;
import model.Player;
import model.Team;
import model.Match;
import model.Owner;
import model.Stadium;

public class DatabaseUtil {
	public static EntityManagerFactory entityManagerFactory;
	public static EntityManager entityManager;

	// Singleton
	private static DatabaseUtil instance = new DatabaseUtil();

	/**
	 * Private constructor for DatabaseUtil to implement the Singleton design pattern.
	 */
	private DatabaseUtil() {}

	/**
	 * Returns a static instance of DatabaseUtil.
	 * @return
	 */
	public static DatabaseUtil getInstance() {
		return instance;
	}

	/**
	 * Sets up the entity manager.
	 * 
	 * @throws Exception
	 */
	public void setUp() throws Exception {
		entityManagerFactory = Persistence.createEntityManagerFactory("FootballLeague");
		entityManager = entityManagerFactory.createEntityManager();
	}

	public void saveTeam(Team team) {
		entityManager.persist(team);
	}

	public void startTransaction() {
		entityManager.getTransaction().begin();
	}

	public void commitTransaction() {
		entityManager.getTransaction().commit();
	}

	public void closeEntityManager() {
		entityManager.close();
	}

	/**
	 * Returns a list of all the teams in the database.
	 * 
	 * @return
	 */
	public List<Team> teamsList() {
		List<Team> teamsList = (List<Team>) entityManager.createQuery("SELECT t FROM Team t", Team.class)
				.getResultList();
		return teamsList;
	}

	/**
	 * Returns a list of all the players in the database.
	 * 
	 * @return
	 */
	public List<Player> playerList() {
		List<Player> playerList = (List<Player>) entityManager.createQuery("SELECT t FROM Player t", Player.class)
				.getResultList();
		return playerList;
	}

	/**
	 * Returns a list of all the players that belong to a particular team.
	 * 
	 * @return
	 */
	public List<Player> playerList(Team team) {
		List<Player> playerList = (List<Player>) entityManager.createQuery("SELECT t FROM Player t", Player.class)
				.getResultList();
		List<Player> aux = new ArrayList<Player>();
		for (Player a : playerList) {
			if (a.getTeam() == team)
				aux.add(a);
		}
		return aux;
	}

	/**
	 * Returns the logo of the team.
	 * 
	 * @param team
	 * @return
	 */
	public Image showTeamLogo(Team team) {
		Image img = new Image(new ByteArrayInputStream(team.getPhoto()));
		return img;
	}

	/**
	 * Returns the player's photo.
	 * 
	 * @param player
	 * @return
	 */
	public Image showPlayerPhoto(Player player) {
		Image img = new Image(new ByteArrayInputStream(player.getPhoto()));
		return img;
	}

	/**
	 * Returns a photo of the stadium.
	 * 
	 * @param stadium
	 * @return
	 */
	public Image showStadiumPhoto(Stadium stadium) {
		Image img = new Image(new ByteArrayInputStream(stadium.getPhoto()));
		return img;
	}

	public void printAllTeamsFromDatabase() {
		List<Team> results = (List<Team>) entityManager
				.createNativeQuery("Select * from FootballLeague.Teams", Team.class).getResultList();
		for (Team team : results) {
			System.out.println(team.getIdTeam() + " " + team.getTeamName() + " Points: " + team.getTeamPoints()
					+ " Position: " + team.getTeamPosition() + " Manager: " + team.getCoachName());
		}
	}

	/**
	 * Returns a list of all the matches's info in the database.
	 * 
	 * @return
	 */
	public List<Match> matchList() {
		List<Match> matchList = (List<Match>) entityManager.createQuery("SELECT t FROM Match t", Match.class)
				.getResultList();
		return matchList;
	}

	/**
	 * Returns a list of all the stadiums in the database.
	 * 
	 * @return
	 */
	public List<Stadium> stadiumList() {
		List<Stadium> stadiumList = (List<Stadium>) entityManager.createQuery("SELECT t FROM Stadium t", Stadium.class)
				.getResultList();
		return stadiumList;
	}

	/**
	 * Returns a list of all the owners in the database.
	 * 
	 * @return
	 */
	public List<Owner> ownerList() {
		List<Owner> ownerList = (List<Owner>) entityManager.createQuery("SELECT t FROM Owner t", Owner.class)
				.getResultList();
		return ownerList;
	}
}
