package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the teams database table.
 * 
 */
@Entity
@Table(name="teams")
@NamedQuery(name="Team.findAll", query="SELECT t FROM Team t")
public class Team implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idTeam;

	private String coachName;

	@Lob
	private byte[] teamLogo;

	private String teamName;

	private int teamPoints;

	private int teamPosition;

	//bi-directional many-to-one association to Player
	@OneToMany(mappedBy="team")
	private List<Player> players;

	//bi-directional many-to-one association to League
	@ManyToOne
	@JoinColumn(name="idLeague")
	private League league;

	//bi-directional one-to-one association to Match
	@OneToOne(mappedBy="team")
	private Match match;

	//bi-directional one-to-one association to Owner
	@OneToOne(mappedBy="team")
	private Owner owner;

	//bi-directional one-to-one association to Stadium
	@OneToOne(mappedBy="team")
	private Stadium stadium;

	public Team() {
	}

	public int getIdTeam() {
		return this.idTeam;
	}

	public void setIdTeam(int idTeam) {
		this.idTeam = idTeam;
	}

	public String getCoachName() {
		return this.coachName;
	}

	public void setCoachName(String coachName) {
		this.coachName = coachName;
	}

	public byte[] getPhoto() {
		return this.teamLogo;
	}

	public void setTeamLogo(byte[] teamLogo) {
		this.teamLogo = teamLogo;
	}

	public String getTeamName() {
		return this.teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public int getTeamPoints() {
		return this.teamPoints;
	}

	public void setTeamPoints(int teamPoints) {
		this.teamPoints = teamPoints;
	}

	public int getTeamPosition() {
		return this.teamPosition;
	}

	public void setTeamPosition(int teamPosition) {
		this.teamPosition = teamPosition;
	}

	public List<Player> getPlayers() {
		return this.players;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}

	public Player addPlayer(Player player) {
		getPlayers().add(player);
		player.setTeam(this);

		return player;
	}

	public Player removePlayer(Player player) {
		getPlayers().remove(player);
		player.setTeam(null);

		return player;
	}

	public League getLeague() {
		return this.league;
	}

	public void setLeague(League league) {
		this.league = league;
	}

	public Match getMatch() {
		return this.match;
	}

	public void setMatch(Match match) {
		this.match = match;
	}

	public Owner getOwner() {
		return this.owner;
	}

	public void setOwner(Owner owner) {
		this.owner = owner;
	}

	public Stadium getStadium() {
		return this.stadium;
	}

	public void setStadium(Stadium stadium) {
		this.stadium = stadium;
	}

}