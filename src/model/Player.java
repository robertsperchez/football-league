package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the players database table.
 * 
 */
@Entity
@Table(name="players")
@NamedQuery(name="Player.findAll", query="SELECT p FROM Player p")
public class Player implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idPlayer;

	@Temporal(TemporalType.DATE)
	private Date playerBirthday;

	private int playerHeight;

	private int playerMarketValue;

	private String playerName;

	private String playerNationality;

	@Lob
	private byte[] playerPhoto;

	private String playerPosition;

	private int playerSalaryYearEuros;

	//bi-directional many-to-one association to Team
	@ManyToOne
	@JoinColumn(name="idPlayerTeam")
	private Team team;

	public Player() {
	}

	public int getIdPlayer() {
		return this.idPlayer;
	}

	public void setIdPlayer(int idPlayer) {
		this.idPlayer = idPlayer;
	}

	public Date getPlayerBirthday() {
		return this.playerBirthday;
	}

	public void setPlayerBirthday(Date playerBirthday) {
		this.playerBirthday = playerBirthday;
	}

	public int getPlayerHeight() {
		return this.playerHeight;
	}

	public void setPlayerHeight(int playerHeight) {
		this.playerHeight = playerHeight;
	}

	public int getPlayerMarketValue() {
		return this.playerMarketValue;
	}

	public void setPlayerMarketValue(int playerMarketValue) {
		this.playerMarketValue = playerMarketValue;
	}

	public String getPlayerName() {
		return this.playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public String getPlayerNationality() {
		return this.playerNationality;
	}

	public void setPlayerNationality(String playerNationality) {
		this.playerNationality = playerNationality;
	}

	public byte[] getPhoto() {
		return this.playerPhoto;
	}

	public void setPlayerPhoto(byte[] playerPhoto) {
		this.playerPhoto = playerPhoto;
	}

	public String getPlayerPosition() {
		return this.playerPosition;
	}

	public void setPlayerPosition(String playerPosition) {
		this.playerPosition = playerPosition;
	}

	public int getPlayerSalaryYearEuros() {
		return this.playerSalaryYearEuros;
	}

	public void setPlayerSalaryYearEuros(int playerSalaryYearEuros) {
		this.playerSalaryYearEuros = playerSalaryYearEuros;
	}

	public Team getTeam() {
		return this.team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

}