package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the stadiums database table.
 * 
 */
@Entity
@Table(name="stadiums")
@NamedQuery(name="Stadium.findAll", query="SELECT s FROM Stadium s")
public class Stadium implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idTeam;

	private int stadiumCapacity;
	
	private String stadiumName;

	@Lob
	private byte[] stadiumPhoto;

	//bi-directional one-to-one association to Team
	@OneToOne
	@JoinColumn(name="idTeam", insertable = false, updatable = false)
	private Team team;

	public Stadium() {
	}

	public int getIdTeam() {
		return this.idTeam;
	}

	public void setIdTeam(int idTeam) {
		this.idTeam = idTeam;
	}

	public int getStadiumCapacity() {
		return this.stadiumCapacity;
	}

	public void setStadiumCapacity(int stadiumCapacity) {
		this.stadiumCapacity = stadiumCapacity;
	}

	public byte[] getPhoto() {
		return this.stadiumPhoto;
	}

	public void setStadiumPhoto(byte[] stadiumPhoto) {
		this.stadiumPhoto = stadiumPhoto;
	}

	public Team getTeam() {
		return this.team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	public String getStadiumName() {
		return stadiumName;
	}

	public void setStadiumName(String stadiumName) {
		this.stadiumName = stadiumName;
	}

}