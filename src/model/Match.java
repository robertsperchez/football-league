package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the matches database table.
 * 
 */
@Entity
@Table(name="matches")
@NamedQuery(name="Match.findAll", query="SELECT m FROM Match m")
public class Match implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idTeam;

	private int matchesDrew;

	private int matchesLost;

	private int matchesPlayed;

	private int matchesWon;

	//bi-directional one-to-one association to Team
	@OneToOne
	@JoinColumn(name="idTeam", insertable = false, updatable = false)
	private Team team;

	public Match() {
	}

	public int getIdTeam() {
		return this.idTeam;
	}

	public void setIdTeam(int idTeam) {
		this.idTeam = idTeam;
	}

	public int getMatchesDrew() {
		return this.matchesDrew;
	}

	public void setMatchesDrew(int matchesDrew) {
		this.matchesDrew = matchesDrew;
	}

	public int getMatchesLost() {
		return this.matchesLost;
	}

	public void setMatchesLost(int matchesLost) {
		this.matchesLost = matchesLost;
	}

	public int getMatchesPlayed() {
		return this.matchesPlayed;
	}

	public void setMatchesPlayed(int matchesPlayed) {
		this.matchesPlayed = matchesPlayed;
	}

	public int getMatchesWon() {
		return this.matchesWon;
	}

	public void setMatchesWon(int matchesWon) {
		this.matchesWon = matchesWon;
	}

	public Team getTeam() {
		return this.team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

}