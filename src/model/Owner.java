package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the owners database table.
 * 
 */
@Entity
@Table(name="owners")
@NamedQuery(name="Owner.findAll", query="SELECT o FROM Owner o")
public class Owner implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idOwner;

	private double ownerBudget;

	private String ownerName;

	//bi-directional one-to-one association to Team
	@OneToOne
	@JoinColumn(name="idOwner", insertable = false, updatable = false)
	private Team team;

	public Owner() {
	}

	public int getIdOwner() {
		return this.idOwner;
	}

	public void setIdOwner(int idOwner) {
		this.idOwner = idOwner;
	}

	public double getOwnerBudget() {
		return this.ownerBudget;
	}

	public void setOwnerBudget(double ownerBudget) {
		this.ownerBudget = ownerBudget;
	}

	public String getOwnerName() {
		return this.ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public Team getTeam() {
		return this.team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

}