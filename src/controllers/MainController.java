package controllers;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.Team;
import model.Player;
import model.Match;
import model.Owner;
import model.Stadium;
import util.DatabaseUtil;

public class MainController implements Initializable {

	@FXML
	private ListView<String> listViewTeams;

	@FXML
	private ListView<String> listViewPlayers;

	@FXML
	private ImageView teamLogoView;

	@FXML
	private ImageView playerPhotoView;

	@FXML
	private ImageView stadiumView;

	@FXML
	private Text showStadiumName;

	@FXML
	private Text showTeamPoints;

	@FXML
	private Text showTeamPosition;

	@FXML
	private Text showMatchesPlayed;

	@FXML
	private Text showMatchesWon;

	@FXML
	private Text showMatchesDrew;

	@FXML
	private Text showMatchesLost;

	@FXML
	private Text showPosition;

	@FXML
	private Text showNationality;

	@FXML
	private Text showHeight;

	@FXML
	private Text showBirthday;

	@FXML
	private Text showSalary;

	@FXML
	private Text showOwnerName;

	@FXML
	private Text showManager;

	@FXML
	private Text showStadiumCapacity;

	@FXML
	private Text showMarketValue;

	@FXML
	private Text showOwnerBudget;

	/**
	 * Interfaces that will be used for Lambdas. It will take numbers(such as salary's and player height) and show them more properly
	 */
	interface processNumbers {
		String number(int nr);
	}

	/**
	 * Same as processNumbers, but for the owner's budget
	 */
	interface processBudget {
		String number(double nr);
	}

	/**
	 * It will populate the list views in the application. First, the teams will appear, and when one team is clicked, 
	 * that team's player's will be shown in the other list view, and also the info about that team's matches, owner and stadium will appear.
	 * When a player is clicked, it will show that player's info.
	 * @throws Exception
	 */
	public void populateListsViews() throws Exception {
		DatabaseUtil db = DatabaseUtil.getInstance();
		db.setUp();
		db.startTransaction();
		// lambda expressions
		processNumbers salary = (nr) -> {
			String result = "";
			float newSalary = nr;
			newSalary /= 1000000;
			result = Float.toString(newSalary) + " millions";
			return result;
		};

		processNumbers marketValue = (nr) -> {
			String result = "";
			float newMarketValue = nr;
			newMarketValue /= 1000000;
			result = Float.toString(newMarketValue) + " millions";
			return result;
		};

		processNumbers height = (nr) -> {
			String result = "";
			float newHeight = nr;
			newHeight /= 100;
			result = Float.toString(newHeight);
			return result;
		};

		processBudget budget = (nr) -> {
			String result = "";
			double newBudget = nr;
			newBudget /= 1000000000;
			result = Double.toString(newBudget) + " billion �";
			return result;
		};

		processNumbers capacity = (nr) -> {
			String result = "";
			float newCapacity = nr;
			newCapacity /= 1000;
			result = Float.toString(newCapacity) + " seats";
			return result;
		};

		// populate the teams names list
		List<Team> teamsDBList = (List<Team>) db.teamsList();
		ObservableList<String> teamsNamesList = getTeamsNames(teamsDBList);
		listViewTeams.setItems(teamsNamesList);
		listViewTeams.refresh();

		// populate the players names list when a team is clicked
		listViewTeams.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				// gets the team that was clicked
				Team selectedTeam = new Team();
				List<Match> matchDBList = (List<Match>) db.matchList();
				List<Stadium> stadiumDBList = (List<Stadium>) db.stadiumList();
				List<Owner> ownerDBList = (List<Owner>) db.ownerList();
				for (Team a : teamsDBList) {
					if (listViewTeams.getSelectionModel().getSelectedItem() == a.getTeamName()) {
						selectedTeam = a;
					}
				}
				// show the players
				List<Player> playerDBList = (List<Player>) db.playerList(selectedTeam);
				ObservableList<String> playersNamesList = getPlayersNames(playerDBList);
				listViewPlayers.setItems(playersNamesList);
				listViewPlayers.refresh();
				// shows the stadium name
				for (Stadium a : stadiumDBList)
					if (selectedTeam.getIdTeam() == a.getIdTeam()) {
						showStadiumName.setText(a.getStadiumName());
						// shows the stadium's photo
						stadiumView.setImage(db.showStadiumPhoto(a));
						// shows the stadium's capacity
						showStadiumCapacity.setText(capacity.number(a.getStadiumCapacity()));
					}
				// shows the owner's name
				for (Owner a : ownerDBList)
					if (selectedTeam.getIdTeam() == a.getIdOwner()) {
						showOwnerName.setText(a.getOwnerName());
						// shows the owner's budget
						showOwnerBudget.setText(budget.number(a.getOwnerBudget()));
					}
				// shows the team's points
				showTeamPoints.setText(Integer.toString(selectedTeam.getTeamPoints()));
				// shows the team's position
				showTeamPosition.setText(Integer.toString(selectedTeam.getTeamPosition()));
				// shows the team's coach
				showManager.setText(selectedTeam.getCoachName());
				// shows the matches played
				for (Match a : matchDBList)
					if (selectedTeam.getIdTeam() == a.getIdTeam()) {
						showMatchesPlayed.setText(Integer.toString(a.getMatchesPlayed()));
						// shows the number of wins
						showMatchesWon.setText(Integer.toString(a.getMatchesWon()));
						// shows the number of losses
						showMatchesLost.setText(Integer.toString(a.getMatchesLost()));
						// shows the number of draws
						showMatchesDrew.setText(Integer.toString(a.getMatchesDrew()));
					}
				// show the team logo
				teamLogoView.setImage(db.showTeamLogo(selectedTeam));
			}

		});

		listViewPlayers.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				// gets the player that was clicked
				List<Player> playerDBList = (List<Player>) db.playerList();
				Player selectedPlayer = new Player();
				for (Player a : playerDBList) {
					if (listViewPlayers.getSelectionModel().getSelectedItem() == a.getPlayerName()) {
						selectedPlayer = a;
					}
				}
				// shows the position
				showPosition.setText(selectedPlayer.getPlayerPosition());
				// shows the nationality
				showNationality.setText(selectedPlayer.getPlayerNationality());
				// shows the birthday
				DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				showBirthday.setText(dateFormat.format(selectedPlayer.getPlayerBirthday()));
				// shows the height
				showHeight.setText(height.number(selectedPlayer.getPlayerHeight()));
				// shows the salary
				showSalary.setText(salary.number(selectedPlayer.getPlayerSalaryYearEuros()));
				// shows the player's photo
				playerPhotoView.setImage(db.showPlayerPhoto(selectedPlayer));
				// shows the player's market value
				showMarketValue.setText(marketValue.number(selectedPlayer.getPlayerMarketValue()));
			}

		});
		// db.closeEntityManager();
	}

	/**
	 * Gets the team's names to show them in the listView 
	 */
	public ObservableList<String> getTeamsNames(List<Team> teams) {
		ObservableList<String> names = FXCollections.observableArrayList();
		for (Team a : teams) {
			names.add(a.getTeamName());
		}
		return names;
	}

	/**
	 * Gets the player's names to show them in the listView 
	 */
	public ObservableList<String> getPlayersNames(List<Player> players) {
		ObservableList<String> names = FXCollections.observableArrayList();
		for (Player a : players) {
			names.add(a.getPlayerName());
		}
		return names;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			populateListsViews();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
