package controllers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class LoginController implements Initializable {

	@FXML
	private Button loginButton;

	@FXML
	private Text loginMessage;

	@FXML
	private TextField usernameInput;

	@FXML
	private PasswordField passwordInput;

	private Socket socket;
	private BufferedReader serverResponse;
	private PrintWriter loginDataProvider;
	private String loginCorrect = "Login info correct!";

	/**
	 * Creates a new socket, which we'll use to transmit the username and password to the server.
	 * The login info will be transmitted when the loginButton is clicked, at which point the server will check the info.
	 * If it's correct, it will change the scene. Otherwise, it will change the login message.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			socket = new Socket("localhost", 1000);
			serverResponse = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			loginDataProvider = new PrintWriter(socket.getOutputStream(), true);
		} catch (IOException e) {
			e.printStackTrace();
		}
		loginButton.setOnMouseClicked(event -> {
			String username = usernameInput.getText();
			String password = passwordInput.getText();

			loginDataProvider.println(username);
			loginDataProvider.println(password);

			try {
				if (serverResponse.readLine().equals(loginCorrect)) {
					Stage primaryStage;
					Parent root;
					if (event.getSource() == loginButton) {
						primaryStage = (Stage) loginButton.getScene().getWindow();
						root = FXMLLoader.load(getClass().getResource("MainView.fxml"));
						Scene sceneMain = new Scene(root, 1200, 590);
						primaryStage.setScene(sceneMain);
						primaryStage.show();
					}
				} else {
					loginMessage.setText("Incorrect password/username!");
				}
			} catch (IOException e) {
				System.out.println("Couldn't create a new window!");
			}
		});
	}

}
