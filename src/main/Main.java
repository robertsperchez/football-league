package main;

import java.io.IOException;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import model.Team;
import util.DatabaseUtil;

public class Main extends Application {

	/**
	 * Sets the starting scene to the login screen. Also sets the window size, title, and shows the scene.
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {
		try {
			Parent root = (AnchorPane) FXMLLoader.load(getClass().getResource("/controllers/LoginScreen.fxml"));
			Scene sceneLogin = new Scene(root, 420, 255);
			primaryStage.setTitle("Premier League Database");
			primaryStage.setScene(sceneLogin);
			primaryStage.show();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}
}
